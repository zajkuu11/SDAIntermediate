package regex;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {

    public void parseLine(String lineToParse) throws Exception {

        String pattern = "(\\d+)\\s+(\\w+)\\s+(.+)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(lineToParse);
        m.matches();
        System.out.println("Line number: " + m.group(1) + " Instruction: " + m.group(2) + " Argument: " + m.group(3));
        System.out.println();
    }

    public void parseExternalInstruction(String path) throws Exception {
        List<Instruction> instruction = new LinkedList<>();

        String pattern = "(\\d+)\\s+(\\w+)\\s+(.+)";
        Pattern r = Pattern.compile(pattern);

        String line;
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            line = scanner.nextLine();
            Matcher m = r.matcher(line);
            m.matches();
            instruction.add(new Instruction((long) Integer.parseInt(m.group(1)), m.group(2), m.group(3)));
        }
        for (Instruction instruction1 : instruction) {
            System.out.print("Line number: " + instruction1.getLineNumber() + " Instruction: " + instruction1.getName() + " Argument: " + instruction1.getArguments());
            System.out.println();
        }
    }
}
