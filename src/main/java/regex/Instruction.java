package regex;

public class Instruction {
    private long lineNumber;
    private String name;
    private String arguments;

    public Instruction(long lineNumber, String name, String arguments) {
        this.lineNumber = lineNumber;
        this.name = name;
        this.arguments = arguments;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public String getName() {
        return name;
    }

    public String getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return "Line number: " + lineNumber + " Instruction: " +name + " Argument: " +arguments ;
    }
}
