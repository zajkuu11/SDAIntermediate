package semiAdvanced;

import java.time.LocalDateTime;

public abstract class Animal {
    private LocalDateTime lastMealTime = LocalDateTime.now();
    private int weight;
    private String name;
    private Playground playground;


    public Animal(int weight, String name) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isAlive() {
        return getLastMealTime().isAfter(LocalDateTime.now().minusDays(10));
    }

    public LocalDateTime getLastMealTime() {
        return lastMealTime;
    }

    public void eat() throws IDontEatException {
        lastMealTime = LocalDateTime.now();
    }

    public void display() {
        System.out.println(getClass().getSimpleName());
        System.out.println(getWeight());
    }


    public void setPlayground(Playground playground) {
        this.playground = playground;
    }

    public Playground getPlayground() {
        return playground;
    }
}
