package semiAdvanced;

public class InjurableStrategy implements AttackStrategy {
    @Override
    public void attacker(Animal attacker, Animal defender) {
        System.out.println(attacker.getClass().getSimpleName() + " zranil " + defender.getClass().getSimpleName());
    }
}
