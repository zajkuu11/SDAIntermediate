package semiAdvanced;

public class BlackBear extends Bear {
    public BlackBear(int weight, String name) {
        super(weight, name, new InjurableStrategy());
    }

}
