package semiAdvanced;

public class BrownBear extends Bear {
    public BrownBear(int weight, String name) {
        super(weight, name, new InjurableStrategy());
    }
}
