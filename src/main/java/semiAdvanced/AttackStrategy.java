package semiAdvanced;

public interface AttackStrategy {
    void attacker(Animal attacker, Animal defender);
}
