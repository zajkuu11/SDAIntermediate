package semiAdvanced;

public interface Attackable {
    void attack(Animal animal);

}
