package semiAdvanced;

public class Bear extends Animal implements Attackable {
    private AttackStrategy attackStrategy;
    private Playground playground;

    public Bear(int weight, String name, AttackStrategy attackStrategy) {
        super(weight, name);
        this.attackStrategy = attackStrategy;
    }

    @Override
    public void attack(Animal animal) {
        attackStrategy.attacker(this, animal);
    }




}
