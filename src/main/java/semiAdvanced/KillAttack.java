package semiAdvanced;

public class KillAttack implements AttackStrategy {


    @Override
    public void attacker(Animal attacker, Animal defender) {
        System.out.println(attacker.getClass().getSimpleName() + " zabil " + defender.getClass().getSimpleName());
    }

}
