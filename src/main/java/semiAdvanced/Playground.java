package semiAdvanced;

import java.util.HashMap;
import java.util.Map;

public class Playground {
    private Map<String, Animal> participants = new HashMap<>();

    public void register(Animal animal) {
        participants.put(animal.getName(), animal);
        animal.setPlayground(this);
    }

    public void fight(Bear bear, String defender) {
        bear.attack(participants.get(defender));
    }

}
