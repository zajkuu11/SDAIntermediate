package semiAdvanced;

import java.time.LocalDateTime;

public class TeddyBear extends Animal {
    public TeddyBear(int weight, String name) {
        super(weight, name);
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public void eat() throws IDontEatException{
        throw new IDontEatException();
    }
}
